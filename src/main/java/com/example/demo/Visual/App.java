package com.example.demo.Visual;

import com.example.demo.Dominio.Cliente;
import com.example.demo.Dominio.Repository;

import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.Button;
import com.vaadin.ui.*;
import com.vaadin.ui.TextField;
import org.springframework.beans.factory.annotation.Autowired;

import java.awt.*;

/**
 * Created by Guicho on 15/07/2017.
 */
@SpringUI
public class App extends UI {
    @Autowired
    Repository estudianteRepository;

    Button Cliente = new Button("Ingresar nombre");
    Button apellidos = new Button("apellidos");
    @Override
    protected void init (VaadinRequest vaadinRequest){
        VerticalLayout Vlayout = new VerticalLayout();
        //HorizontalLayout vlayout = new HorizontalLayout();
        TextField nombre = new TextField("Nombre");
      //  TextField autor = new TextField("");
        TextField apellido =new TextField("Apellido");
        TextField edad =new TextField("Edad");


        Grid<com.example.demo.Dominio.Cliente> grid = new Grid<>();
        grid.addColumn(com.example.demo.Dominio.Cliente::getId).setCaption("ID");
        grid.addColumn(com.example.demo.Dominio.Cliente::getNombre).setCaption("Nombre");
        grid.addColumn(com.example.demo.Dominio.Cliente::getApellido).setCaption("Apellido");
        grid.addColumn(com.example.demo.Dominio.Cliente::getEdad).setCaption("Edad");
        //grid.addColumn(Libro::getEditorial).setCaption("Curso");


        Button add = new Button("Ingresar los clientes");
        add.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                Cliente e = new Cliente();
                e.setNombre(nombre.getValue());
             //   e.setApellido(apellidos.getValue());
                e.setEdad(apellido.getValue());
               // e.setCurso(curso.getValue());
                e.setEdad(edad.getValue());

                estudianteRepository.save(e);
                grid.setItems(estudianteRepository.findAll());

                nombre.clear();
                apellido.clear();
                edad.clear();


            }
        });

        Button rem = new Button("Eliminar clientes");


        Vlayout.addComponents(nombre,apellido,edad,add,rem);

        //Vlayout.addComponentsAndExpand(Vlayout);
        //Vlayout.addComponentsAndExpand(grid);
        Vlayout.addComponent(grid );
        //new Dimension(50,20);
        //hlayout.setComponentAlignment(add, Alignment.BOTTOM_LEFT);

        setContent(Vlayout);
    }
}
