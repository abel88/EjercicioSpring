package com.example.demo.Dominio;

/**
 * Created by Ventas on 21/07/2017.
 */
public class Proveedor {
    String nombreprov;
    String apellidoprov;
    String direccion;
    int telefono;

    public Proveedor(String nombreprov, String apellidoprov, String direccion, int telefono) {
        this.nombreprov = nombreprov;
        this.apellidoprov = apellidoprov;
        this.direccion = direccion;
        this.telefono = telefono;

    }

    public Proveedor() {
    }

    public String getNombreprov() {
        return nombreprov;
    }

    public void setNombreprov(String nombreprov) {
        this.nombreprov = nombreprov;
    }

    public String getApellidoprov() {
        return apellidoprov;
    }

    public void setApellidoprov(String apellidoprov) {
        this.apellidoprov = apellidoprov;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }
}

