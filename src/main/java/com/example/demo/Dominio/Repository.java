package com.example.demo.Dominio;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Guicho on 15/07/2017.
 */
public interface Repository extends JpaRepository <Cliente,Long>{
}
