package com.example.demo.Dominio;

/**
 * Created by Ventas on 21/07/2017.
 */
public class Producto {
    String nombrePro;
    String descripicioPro;
    float  precio;

    public Producto() {
    }

    public Producto(String nombrePro, String descripicioPro, float precio) {
        this.nombrePro = nombrePro;
        this.descripicioPro = descripicioPro;
        this.precio = precio;
    }

    public String getNombrePro() {
        return nombrePro;
    }

    public void setNombrePro(String nombrePro) {
        this.nombrePro = nombrePro;
    }

    public String getDescripicioPro() {
        return descripicioPro;
    }

    public void setDescripicioPro(String descripicioPro) {
        this.descripicioPro = descripicioPro;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }
}
